# sterlib

A compact, unopinionated library for interfacing with Sterling. 

## How to use:

Some enablers to watch out for:

 - You will need to setup the transmissions with Enterprise Managed File Transfer Services (the group that manages Sterling). To learn more about this process, read [this](https://wiki.paychex.com/pages/viewpage.action?pageId=92412615) wiki.  
 - If you want to request your own namespace from Storage Engineering, read [this](https://wiki.paychex.com/pages/viewpage.action?pageId=141427162) wiki page.
 - The development Sterling system (`sifgsdvw1.paychex.com`) can only pull files from your namespace if it is located on the Lab ECS instance (`objlab.paychex.com`), The easiest way to get around this is by creating your own bucket in the `selfserve` namespace and using that until you are ready to hit the N1 Sterling endpoints. You can learn more about the self-service ECS namespace [here](https://wiki.paychex.com/pages/viewpage.action?pageId=101798718). Of course, this only applies if you plan on using your own namespace instead of Sterling's.
 - Almost all Paychex app servers will fail to connect over SFTP **unless** you turn off strick host key checking.
 
Note: **all** configuration parameters must be in string form. 

### ECS

Usage of sterlib is actually quite simple. Create a `java.util.Map<String, String>` and put your configuration parameters in there. 

At the bare minimum, you need the following parameters set for an ECS submission:

```java
Map<String, String> config = new HashMap<>();
config.put("method", "Ecs");
config.put("destinationFilename", "outboundFileName");
config.put("sterlingMailboxPath", "sterlingMailboxPath");
```

The Sterling endpoint URL defaults to the dev one, so you'll want to change it for test and production releases:

```java
config.put("sterlingEndpointUrl", "http://<sterling_hostname>.paychex.com:<some_port>");
```

By default, the ECS implementation of sterlib will use Sterling's namespace for the storage of the file. Optionally, you can configure sterlib to use your own:

```java
config.put("ecsEndpointUrl", "https://obj.paychex.com:9021");
config.put("ecsBucketName", "myBucket");
config.put("ecsAccessKeyId", "myAccessId");
config.put("ecsSecretAccessKey", "mySecretKey");
```

If the upload to your namespace fails for whatever reason, sterlib will attempt to upload it to Sterling's. You can turn this behavior off is you want:

```java
config.put("ecsFallbackToSterlingNamespace", "false");
```

### SFTP

Likewise for SFTP, there is a set of bare minimum parameters to be set:

```java
Map<String, String> config = new HashMap<>();
config.put("method", "Sftp");
config.put("destinationServer", "some.sterling.hostname");
config.put("destinationFilename", "outboundFileName");
config.put("privateKeyPath", "/path/to/private/key");
```

Most of the optional configuration items for SFTP are highly recommended. While the library itself won't give you an error if you don't provide these values, more than likely the default values won't suffice for any real-world usage.

The `known_hosts` file location is by default set to `~/.ssh/known_hosts`. You can change this value here:

```java
config.put("knownHostsPath", "/path/to/known/hosts/file");
```

The timeout is set to 30 seconds, but you can change that here (note: the timeout is in **seconds**)

```java
config.put("timeout", "someTimeout");
```

The user that will be used to log into Sterling will most likely need to be set. Here, it defaults to the user on the local machine:

```java
config.put("user", "sterlingUser");
```

Same goes for the Sterling mailbox path. By default it is set to the remote user's home directory (`~`), but you can set that like so:

```java
config.put("sterlingMailboxPath", "/path/to/dest/Sterling/directory");
```

By default, the port is set to 22 (the standard SSH port). Sterling usually uses something different:

```java
config.put("destinationPort", "12345");
```

By default, sterlib strictly checks all host keys of the machines it is connecting to. If you want to disable this behavior (likely you will at Paychex, since most app servers don't have the Sterling host keys installed), you can:
```java
config.put("strictHostKeyChecking", "false");
```

### Calling the library

Once you have your configuration map set up, you can call sterlib. 

Sterlib requires a couple things: a configution map (`Map<String, String>`) and some sort of data (in for the form of an `InputStream`). For example:

```java
Map<String, String> config = new HashMap<String, String>();
... insert config properties here ...

InputStream data = new ByteArrayInputStream("Hello world!".getBytes(StandardCharsets.UTF_8));

Sterlib.push(config, data)
```

Sterlib will automatically retry a failed transmission (independent of transmission method). By default, it will retry twice (for a total of 3 attempts), waiting 30 seconds in between attempt. You can configure the number of retries as well as the waiting time:

```java
int retries = 1;
int retryInterval = 10;
Sterlib.push(config, data, retries, retryInterval)
```

