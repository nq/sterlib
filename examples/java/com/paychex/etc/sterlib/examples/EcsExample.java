package com.paychex.etc.sterlib.examples;

import com.paychex.etc.sterlib.Sterlib;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EcsExample {
    public static void pushToSterlibViaEcs() {

        // The interface for Sterling calls for the data portion to be in the form of an InputStream.
        // So we generate a random string and convert it to an InputStream.
        String dataString = UUID.randomUUID().toString();
        InputStream data = new ByteArrayInputStream(dataString.getBytes(StandardCharsets.UTF_8));

        // All of the configuration of Sterlib is done through a map of strings to strings.
        Map<String, String> config = new HashMap<>();


        // ----------------------------
        // Required Parameters
        // ----------------------------

        // This is the name of the transfer method to use ECS to transfer the file to Sterling.
        config.put("method", "Ecs");

        // The filename that Sterling will use to name the file when it transmits the file to the destination partner.
        // Note: This is different than the object name that is used in ECS. The ECS object name is automatically generated in order
        // to prevent naming collisions in ECS. The destination filename, however, will be added to the ECS object metadata.
        // Required.
        config.put("destinationFilename", "testFilename");

        // This information is provided to you by Security after you fill out their SEC009 form.
        // Required
        config.put("sterlingMailboxPath", "sterling/mailbox/path");

        // ----------------------------
        // Optional Parameters
        // ----------------------------

        // The Sterling REST endpoint to call when the upload to ECS has been completed. Useful to change for testing purposes.
        // Default is the dev endpoint (http://sifgsdvw1.paychex.com:15510). Change for when going to production.
        config.put("sterlingEndpointUrl", "http://sifgsdvw1.paychex.com:15510");

        // The ECS URL to connect to.
        // Default is the dev objlab (https://objlab.paychex.com:9021).
        config.put("ecsEndpointUrl", "https://ecs.endpoint.url:9021");

        // The ECS namespace to use to find the bucket.
        // Specify this (along with the bucket name, key ID, and access key) if you want to use your own namespace.
        config.put("ecsNamespace", "my-namespace");

        // The ECS bucket to use to store the file.
        // Specify this (along with the namespace, key ID, and access key) if you want to use your own namespace.
        config.put("ecsBucketName", "myBucket");

        // The ECS Access Key ID is effectively a username used to connect to ECS.
        // Specify this (along with the namespace, bucket name, and access key) if you want to use your own namespace.
        config.put("ecsAccessKeyId", "keyId");

        // The ECS Secret Access Key is effectively a password that goes along with the Access Key Id.
        // Specify this (along with the namespace, bucket name, and key ID) if you want to use your own namespace.
        config.put("ecsSecretAccessKey", "someSecretKey");







        // ----------------------------
        // Misc. Sterlib Configuration
        // ----------------------------

        // By default, Sterlib will retry a failed transmission twice, for a total of three attempts. You can specify how many
        // retry attempts you want sterling to make, as well as the time (in seconds) between each retry attempt.
        // If the number of retries is set to 0, then the retry interval has no effect.
        int retries = 3;
        int timeBetweenRetriesInSeconds = 10;

        // Make to call to Sterlib! Sterlib will return true if the transmission succeeded, otherwise it will throw an exception.
        boolean result = Sterlib.push(config, data, retries, timeBetweenRetriesInSeconds);
    }
}