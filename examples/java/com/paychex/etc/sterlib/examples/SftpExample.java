package com.paychex.etc.sterlib.examples;

import com.paychex.etc.sterlib.Sterlib;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.io.ByteArrayInputStream;

public class SftpExample {
    public static void pushToSterlib() {

        // The interface for Sterling calls for the data portion to be in the form of an InputStream.
        // So we generate a random string and convert it to an InputStream.
        String dataString = UUID.randomUUID().toString();
        InputStream data = new ByteArrayInputStream(dataString.getBytes(StandardCharsets.UTF_8));

        // All of the configuration of Sterlib is done through a map of strings to strings.
        Map<String, String> config = new HashMap<>();

        // This is the name of the transfer method to use ECS to transfer the file to Sterling.
        config.put("method", "Sftp");


        // ----------------------------
        // Sterling Configuration
        // ----------------------------

        // The Sterling SFTP server to to connect to.
        // Sterling uses two SFTP servers: sfg.paychex.com for production, and sfgst.paychex.com for testing.
        // Required.
        config.put("destinationServer", "destination.server.hostname");

        // The SFTP port to use to connect to the Sterling SFTP server. Sterlib is set to use 22 by default, however
        // Sterling uses port 10522 for both production and test systems.
        config.put("destinationPort", Integer.toString(22));

        // The directory on the Sterlib SFTP server to put the file in.
        // Default is remote user home directory
        config.put("sterlingMailboxPath", "/path/to/destination/file");

        // The filename that Sterling will use to name the file when it transmits the file to the destination partner.
        // Required.
        config.put("destinationFilename", "testFilename");

        // The user used to connect to the Sterling SFTP server.
        // Default is the same username as local user.
        config.put("user", "remoteUsername");

        // The private key used to authenticate with the Sterling SFTP server.
        // Default is ~/.ssh/id_rsa
        config.put("privateKeyLocation", "/path/to/local/user/private/key");

        // The SFTP connection timeout in seconds.
        // Default is 30 seconds.
        config.put("timeout", "30");

        // The known hosts file against which to crosscheck the public key of the Sterling server.
        // Default is ~/.ssh/known_hosts
        config.put("knownHostsPath", "/path/to/local/known_hosts/file");

        // If you don't want to do host key checking, you can disable it. This can be useful if changing the known hosts
        // file required an extraordinary about of work, AND you are CERTAIN that the server you are connecting is actually who
        // it says it is. Please be aware of the serious security implications of setting this to false.
        // For security reasons, the default is true.
        config.put("strictHostKeychecking", "true");


        // ----------------------------
        // Misc. Sterlib Configuration
        // ----------------------------

        // By default, Sterlib will retry a failed transmission twice, for a total of three attempts. You can specify how many
        // retry attempts you want sterling to make, as well as the time (in seconds) between each retry attempt.
        // If the number of retries is set to 0, then the retry interval has no effect.
        int retries = 3;
        int timeBetweenRetriesInSeconds = 10;

        // Make to call to Sterlib! Sterlib will return true if the transmission succeeded, otherwise it will throw an exception.
        boolean result = Sterlib.push(config, data, retries, timeBetweenRetriesInSeconds);
    }
}