import com.amazonaws.util.BinaryUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paychex.etc.sterlib.Sterlib;
import com.paychex.etc.sterlib.methods.Ecs;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.io.IOUtils;
import org.apache.sshd.common.file.nativefs.NativeFileSystemFactory;
import org.apache.sshd.common.keyprovider.FileKeyPairProvider;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.config.keys.AuthorizedKeysAuthenticator;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.gaul.s3proxy.AuthenticationType;
import org.gaul.s3proxy.S3Proxy;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.StorageMetadata;
import org.jclouds.blobstore.options.ListContainerOptions;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Test {
    public static void main(String[] args) throws Exception{
        ecsTest();
    }

    public static void sshTest() throws Exception{
        String hostname = "localhost";
        int sshServerPort = Utils.getFreePort();
        String currentDir = new File(".").getAbsolutePath();
        String user = "test";
        String privateKeyPath = Utils.getFile("client_private_key").toAbsolutePath().toString();
        int timeout = 30;
        String knownHostsPath = Utils.getFile("known_hosts").toAbsolutePath().toString();

        SshServer sshd = SshServer.setUpDefaultServer();
        sshd.setPort(sshServerPort);
        sshd.setKeyPairProvider(new FileKeyPairProvider(Utils.getFile("server_private_key")));
        sshd.setPublickeyAuthenticator(new AuthorizedKeysAuthenticator(Utils.getFile("client_public_key")));
        sshd.setSubsystemFactories(Arrays.asList(new SftpSubsystemFactory()));
        sshd.setFileSystemFactory(new NativeFileSystemFactory());
        sshd.start();

        String filename = "testFilename";
        String dataString = UUID.randomUUID().toString();
        InputStream data = new ByteArrayInputStream(dataString.getBytes(StandardCharsets.UTF_8));
        Map<String, String> config = new HashMap<>();
        config.put("method", "Sftp");
        config.put("destinationServer", hostname);
        config.put("destinationPort", Integer.toString(sshServerPort));
        config.put("destinationDirectory", currentDir);
        config.put("user", user);
        config.put("privateKeyPath", privateKeyPath);
        config.put("timeout", Integer.toString(timeout));
        config.put("knownHostsPath", knownHostsPath);
        config.put("destinationFilename", filename);

        boolean result = Sterlib.push(config, data);

        assertTrue(result);
        assertTrue(Files.exists(Paths.get(filename)));
        assertEquals(
            new BufferedReader(new InputStreamReader(new FileInputStream(Paths.get(filename).toFile())))
                .lines()
                .collect(Collectors.joining("\n")),
            dataString
        );

        // Cleanup
        Files.delete(Paths.get(filename));
        sshd.stop(true);
    }

    public static void sshToEnsDevAppServerTest() throws Exception {
        Map<String, String> config = new HashMap<>();
        config.put("method", "Sftp");
        config.put("destinationServer","ensaptstw1");
        config.put("destinationPort", "22");
        config.put("destinationDirectory", "~/");
        config.put("user", "wlaw");
        config.put("privateKeyLocation", "~/.ssh/client_private_key");
        config.put("timeout", "30");

        InputStream data = new ByteArrayInputStream("Hello world!\n".getBytes(StandardCharsets.UTF_8));

        Sterlib.push(config, data);
    }

    public static void ecsSelfserveNamespaceTest(){
        InputStream data = new ByteArrayInputStream("Hello world!\n".getBytes(StandardCharsets.UTF_8));

        Map<String, String> config = new HashMap<>();
        config.put("method", "Ecs");
        config.put("ecsEndpointUrl", "https://objlab.paychex.com:9021");
        config.put("ecsNamespace", "selfserve");
        config.put("ecsBucketName", "sterlib");
        config.put("ecsAccessKeyId", "wlaw@paychex.com");
        config.put("ecsSecretAccessKey", "H3DEquLf4UVu8UVPIpp+e+QU7MPbfug/VrAZFX3l");
        config.put("destinationFilename", "wlawTestFilename.txt");
        config.put("sterlingMailboxPath", "WLaw_Test");
        config.put("sterlingEndpointUrl", "http://sifgsdvw1.paychex.com:15510");
        config.put("ecsFallbackToSterlingNamespace", "false");
        Sterlib.push(config, data, 0);
    }

    public static void ecsTest() throws Exception {
        String ecsEndpoint = "http://localhost:8080";
        String ecsBucketName = "someBucketName";
        String ecsAccessKeyId = "identity";
        String ecsSecretAccessKey = "credential";
        boolean ecsFallbackToSterlingNamespace = false;

        String destinationFilename = "someFileName";
        String sterlingMailboxPath = "mailbox";
        int sterlingEndpointPort = 8081;
        String sterlingEndpointUrl = "http://localhost:" + sterlingEndpointPort;

        String ecsPresignedBucketName = "presigned";
        String ecsPresignedUrl = "http://localhost:8080/" + ecsPresignedBucketName;

        String dataString = UUID.randomUUID().toString(); // Just generate some random data

        // Setup a local, in-memory S3 compatible object store.
        BlobStoreContext context = ContextBuilder
            .newBuilder("transient")
            .build(BlobStoreContext.class);

        // Create the regular bucket. Also create a separate bucket to simulate uploading to Sterling via pre-signed URL.
        BlobStore s3Backend = context.getBlobStore();
        s3Backend.createContainerInLocation(null, ecsBucketName);
        s3Backend.createContainerInLocation(null, ecsPresignedBucketName);

        // Setup S3Proxy to forward S3 requests from its listener to the local store
        S3Proxy s3Proxy = S3Proxy.builder()
            .blobStore(context.getBlobStore())
            .endpoint(URI.create(ecsEndpoint))
            .awsAuthentication(AuthenticationType.AWS_V2_OR_V4, ecsAccessKeyId, ecsSecretAccessKey)
            .ignoreUnknownHeaders(true)
            .build();

        // Setup a simple HTTP server to listen for the Sterling requests.
        HttpServer sterling = HttpServer.create(new InetSocketAddress(sterlingEndpointPort), 0);
        Utils.SterlingHandler sterlingHandler = new Utils.SterlingHandler();
        sterling.createContext("/file-transfer/get-presigned-url", (HttpExchange exchange) -> {
            exchange.sendResponseHeaders(200, ecsPresignedBucketName.length());
            exchange.getResponseBody().write(ecsPresignedBucketName.getBytes(StandardCharsets.UTF_8));
            exchange.close();
        });
        sterling.createContext("/file-transfer/submit-file", sterlingHandler);
        sterling.setExecutor(null);

        try {
            s3Proxy.start();
            sterling.start();

            InputStream dataStream = new ByteArrayInputStream(dataString.getBytes(StandardCharsets.UTF_8));

            // Setup the Sterlib ECS config.
            Map<String, String> config = new HashMap<>();
            config.put("method", "Ecs");
            config.put("ecsEndpointUrl", ecsEndpoint);
            config.put("ecsBucketName", ecsBucketName);
            config.put("ecsAccessKeyId", ecsAccessKeyId);
            config.put("ecsSecretAccessKey", ecsSecretAccessKey);
            config.put("destinationFilename", destinationFilename);
            config.put("sterlingMailboxPath", sterlingMailboxPath);
            config.put("sterlingEndpointUrl", sterlingEndpointUrl);
            config.put("ecsFallbackToSterlingNamespace", String.valueOf(ecsFallbackToSterlingNamespace));


            // Presto!
            boolean result = Sterlib.push(config, dataStream, 0);
            assertTrue(result);

            // Wait for the REST call to fake sterling to come in. This will keep us from waiting for ECS operations too.
            Utils.HttpRequest requestToSterling = sterlingHandler.waitForLastRequest();

            // Get the metadata of the newly created object directly from the store.
            StorageMetadata md = s3Backend.list(ecsBucketName, ListContainerOptions.Builder.withDetails()).stream()
                .findFirst()
                .orElseGet(null);

            // Get the object data itself, and convert it to a string.
            String resultString = new BufferedReader(new InputStreamReader(s3Backend.getBlob(ecsBucketName, md.getName()).getPayload().openStream()))
                .lines()
                .collect(Collectors.joining("\n"));

            // Assertions about the sterling call
            assertEquals(sterlingMailboxPath, requestToSterling.getDeserializedBody().path("mailboxpath").asText());
            assertTrue("Pre-signed URL contains bucket name", requestToSterling.getDeserializedBody().path("fileurl").asText().contains(ecsBucketName));
            assertTrue("Pre-signed URL contains file URI", requestToSterling.getDeserializedBody().path("fileurl").asText().contains(md.getName()));
            assertEquals(destinationFilename, requestToSterling.getDeserializedBody().path("filename").asText());
            // Convert the eTag from base16 to base64
            assertEquals(BinaryUtils.toBase64(BinaryUtils.fromHex(md.getETag())), requestToSterling.getDeserializedBody().path("filemd5").asText());
            assertEquals(new URI(sterlingEndpointUrl + "/file-transfer/submit-file").getPath(), requestToSterling.getURI().toString());

            // Assertions about the ECS upload.
            assertEquals(dataString, resultString);
            assertEquals(destinationFilename, md.getUserMetadata().get(Ecs.filenameMetadataKeyName()));

        } catch (Exception e){
            throw e;
        } finally {
            s3Proxy.stop();
            sterling.stop(0);
        }

    }


    private static void assertTrue(boolean test) throws TestFailedException{
        if(!test){
            throw new TestFailedException();
        }
    }

    private static void assertTrue(String message, boolean test) throws TestFailedException{
        if(!test){
            throw new TestFailedException(message);
        }
    }

    private static void assertEquals(String expected, String actual) throws TestFailedException{
        if(!((expected != null && actual != null && expected.equals(actual)) || (expected == null && actual == null))){
            throw new TestFailedException();
        }
    }

    public static class Utils {
        public static int getFreePort() throws Exception {
            try (ServerSocket s = new ServerSocket(0)) {
                return s.getLocalPort();
            }
        }

        public static Path getFile(String resource) {
            URL url = Utils.class.getClassLoader().getResource(resource);
            Path path;
            try {
                path = Paths.get(url.toURI());
            } catch(URISyntaxException e) {
                path = Paths.get(url.getPath());
            }
            return path;
        }

        /**
         * Context handler for the simple HTTP server in the unit tests.
         */
        public static class SterlingHandler implements HttpHandler {
            private LinkedBlockingQueue<HttpRequest> requests = new LinkedBlockingQueue<>();

            /**
             * Handle the HTTP request from the Sterlib client call to "Sterling"
             * @param exchange HttpExchange - The HTTP request and response object.
             * @throws IOException - Thrown is something bad happens.
             */
            @Override
            public void handle(HttpExchange exchange) throws IOException {
                String response = "{\"Status\": \"Success\", \"Message\": \"Okay!\"}";
                int responseCode = 200;
                synchronized (this){
                    try {
                        requests.put(new HttpRequest(exchange));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JsonMappingException | JsonParseException e) {
                        e.printStackTrace();
                        response = "{\"Status\": \"Failure\", \"Message\": \"Invalid request: Unable to parse JSON\"}";
                        responseCode = 400;
                    }
                }

                exchange.sendResponseHeaders(responseCode, response.length());
                exchange.getResponseBody().write(response.getBytes(StandardCharsets.UTF_8));
                exchange.close();
            }

            /**
             * Get the most recent request information from the request queue.
             * @return HttpRequest - The last request that was received.
             * @throws InterruptedException - Since this is synchronized and calls a concurrency method, this is required.
             */
            public synchronized HttpRequest waitForLastRequest() throws InterruptedException {
                return requests.poll(10, TimeUnit.SECONDS);
            }
        }

        /**
         * Separates the HTTP request data from the whole HttpExchange object.
         */
        public static class HttpRequest {
            private JsonNode deserializedBody;
            private String body;
            private Headers headers;
            private String method;
            private URI uri;

            /**
             * Create an instance of an HTTP request from the HttpExchange
             * @param exchange HttpExchange - The HTTP request/response object from the handler.
             * @throws IOException - If something bad happens.
             */
            public HttpRequest(HttpExchange exchange) throws IOException{
                this.body = IOUtils.toString(exchange.getRequestBody(), StandardCharsets.UTF_8);
                this.deserializedBody = new ObjectMapper().readTree(body);
                this.headers = exchange.getRequestHeaders();
                this.method = exchange.getRequestMethod();
                this.uri = exchange.getRequestURI();
            }

            public String getBody() {
                return this.body;
            }

            public JsonNode getDeserializedBody(){
                return this.deserializedBody;
            }

            public Headers getHeaders() {
                return this.headers;
            }

            public String getMethod() {
                return this.method;
            }

            public URI getURI() {
                return this.uri;
            }
        }


    }

    public static class TestFailedException extends Exception {
        public TestFailedException() {super();}
        public TestFailedException(String message) {super(message);}
        public TestFailedException(String message, Throwable cause) {super(message, cause);}
        public TestFailedException(Throwable cause) {super(cause);}
    }

}
