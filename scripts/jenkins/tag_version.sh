#!/usr/bin/env bash

REMOTE="origin"
GIT_RELAY=`git remote | grep relay`
if [ ! -z "$GIT_RELAY" ]; then
    REMOTE="relay"
fi

git tag -a $projectVersion -m "version $projectVersion"
git push $REMOTE $projectVersion