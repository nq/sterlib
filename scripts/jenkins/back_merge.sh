#!/usr/bin/env bash

REMOTE="origin"
GIT_RELAY=`git remote | grep relay`
if [ ! -z "$GIT_RELAY" ]; then
    REMOTE="relay"
fi

CURRENT_BRANCH=${1:-`git rev-parse --abbrev-ref HEAD`}
git checkout -b develop $REMOTE/develop
git pull $REMOTE develop
git pull $REMOTE master
git push $REMOTE develop
git checkout $CURRENT_BRANCH
