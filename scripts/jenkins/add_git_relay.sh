#!/usr/bin/env bash
if [ ! -z "$GIT_HOST" ]; then
    GIT_RELAY=`git remote | grep relay`
    if [ ! -z "$GIT_RELAY" ]; then
        git remote remove relay
    fi
    git remote add relay "http://$GIT_HOST:$GIT_PORT/scm/etc/sterlib.git"
fi
