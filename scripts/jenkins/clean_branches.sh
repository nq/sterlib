#!/usr/bin/env bash

REMOTE="origin"
GIT_RELAY=`git remote | grep relay`
if [ ! -z "$GIT_RELAY" ]; then
    REMOTE="relay"
fi

REF=${1:-${GIT_BRANCH:-`git rev-parse --abbrev-ref HEAD`}}
CURRENT_BRANCH=${REF//*\/}
git fetch $REMOTE --prune
git branch -r --merged $REMOTE/$CURRENT_BRANCH | grep $REMOTE | grep -v \* | grep -v master | grep -v dmz | grep -v develop | while read line; do git push $REMOTE :${line//*\/}; done
