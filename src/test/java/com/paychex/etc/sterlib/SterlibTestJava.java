package com.paychex.etc.sterlib;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import com.paychex.etc.sterlib.util.OutputCapturer;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

public class SterlibTestJava {
    private static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Test
    public void test_push_withSetAmountOfRetriesAndRetryInterval() throws Exception {
        OutputCapturer.startCapture();

        Map<String, String> config = new HashMap<>();
        config.put("method", "Log");

        boolean result = Sterlib.push(config, new ByteArrayInputStream("null".getBytes("UTF-8")), 1, 1);
        String capture = OutputCapturer.stopCapture();

        assertTrue(result);
        assertThat(capture, containsString("null"));
    }

    @Test
    public void test_push_withSetAmountOfRetries() throws Exception {
        OutputCapturer.startCapture();

        Map<String, String> config = new HashMap<>();
        config.put("method", "Log");

        boolean result = Sterlib.push(config, new ByteArrayInputStream("null".getBytes("UTF-8")), 1);
        String capture = OutputCapturer.stopCapture();

        assertTrue(result);
        assertThat(capture, containsString("null"));
    }

    @Test
    public void test_push() throws Exception {
        OutputCapturer.startCapture();

        Map<String, String> config = new HashMap<>();
        config.put("method", "Log");

        boolean result = Sterlib.push(config, new ByteArrayInputStream("null".getBytes("UTF-8")));
        String capture = OutputCapturer.stopCapture();

        assertTrue(result);
        assertThat(capture, containsString("null"));
    }
}