package com.paychex.etc.sterlib.util;

public class ThrowMethodException extends Throwable {
    public ThrowMethodException(String message){
        super(message);
    }

}
