package com.paychex.etc.sterlib.util;

import java.io.IOException;
import java.io.OutputStream;

public class TeeOutputStream extends OutputStream {
    private final OutputStream[] streams;

    public TeeOutputStream(final OutputStream... streams){
        this.streams = streams;
    }

    @Override
    public void write(int b) throws IOException {
        for(OutputStream stream: streams){
            stream.write(b);
        }
    }

    @Override
    public void write(byte[] buffer) throws IOException {
        for (OutputStream stream: streams) {
            stream.write(buffer);
        }
    }

    @Override
    public void write(byte[] buffer, int offset, int count) throws IOException {
        for (OutputStream stream: streams) {
            stream.write(buffer, offset, count);
        }
    }

    @Override
    public void close() throws IOException {
        for (OutputStream stream: streams) {
            stream.close();
        }
    }

    @Override
    public void flush() throws IOException {
        for (OutputStream stream: streams) {
            stream.flush();
        }
    }
}