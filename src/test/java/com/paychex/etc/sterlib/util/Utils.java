package com.paychex.etc.sterlib.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Helper classes and functions for the tests.
 */
public class Utils {

    /**
     * Get an unused port from the system.
     * @return int - The open port
     * @throws Exception - Thrown if something bad happens
     */
    public static int getFreePort() throws Exception {
        try (ServerSocket s = new ServerSocket(0)) {
            return s.getLocalPort();
        }
    }

    /**
     * Given a filesystem path as a string, open it as a resource from the resources directory.
     * @param resource String - The resource in question.
     * @return Path - The Path to the resource.
     */
    public static Path getFile(String resource) {
        URL url = Utils.class.getClassLoader().getResource(resource);
        Path path;
        try {
            path = Paths.get(url.toURI());
        } catch(URISyntaxException e) {
            path = Paths.get(url.getPath());
        }
        return path;
    }

    /**
     * Separates the HTTP request data from the whole HttpExchange object.
     */
    public static class HttpRequest {
        private JsonNode deserializedBody;
        private String body;
        private Headers headers;
        private String method;
        private URI uri;

        /**
         * Create an instance of an HTTP request from the HttpExchange
         * @param exchange HttpExchange - The HTTP request/response object from the handler.
         * @throws IOException - If something bad happens.
         */
        public HttpRequest(HttpExchange exchange) throws IOException{
            this.body = IOUtils.toString(exchange.getRequestBody(), StandardCharsets.UTF_8);
            this.deserializedBody = new ObjectMapper().readTree(body);
            this.headers = exchange.getRequestHeaders();
            this.method = exchange.getRequestMethod();
            this.uri = exchange.getRequestURI();
        }

        public String getBody() {
            return this.body;
        }

        public JsonNode getDeserializedBody(){
            return this.deserializedBody;
        }

        public Headers getHeaders() {
            return this.headers;
        }

        public String getMethod() {
            return this.method;
        }

        public URI getURI() {
            return this.uri;
        }
    }
}
