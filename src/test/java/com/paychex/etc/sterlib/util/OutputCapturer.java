package com.paychex.etc.sterlib.util;

import scala.Console;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class OutputCapturer {

    private static PrintStream syso;
    private static PrintStream syserr;
    private static ByteArrayOutputStream captureStream;
    /**
     * Begin capturing the output of stderr (System.err) and stdout (System.out).
     * Call stopCaptureStderr() to get the output when done capturing.
     */
    public static void startCapture(){
        syso = System.out;
        syserr = System.err;

        captureStream = new ByteArrayOutputStream();
        PrintStream capturePrintStream = new PrintStream(captureStream);

        System.setOut(new TeePrintStream(capturePrintStream, syso));
        System.setErr(new TeePrintStream(capturePrintStream, syserr));

        // We need to manually set them for Scala too, because changing System.out or -.err will not change the reference
        // that Scala holds onto.
        Console.setErr(System.err);
        Console.setOut(System.out);
    }

    /**
     * Reset System.err and System.out to their original configurations.
     */
    public static String stopCapture(){
        if(syserr == null || syso == null){
            throw new NullPointerException("Can't set syserr or syso to null. Did you forget to redirect it first?");
        }
        System.setErr(syserr);
        System.setOut(syso);

        // We need to manually set them for Scala too, because changing System.out or -.err will not change the reference
        // that Scala holds onto.
        Console.setErr(System.err);
        Console.setOut(System.out);

        // Null pointer exception will be thrown if captureStream is null, so no need to manually throw it.
        String captureString = captureStream.toString();
        captureStream = null;

        return captureString;
    }

}
