package com.paychex.etc.sterlib

import java.util.Date

import org.junit.Test
import org.hamcrest.CoreMatchers._
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Assert.assertFalse
import org.junit.Assert.assertEquals
import junit.framework.TestCase.fail


class UtilsTest {

  @Test
  def test_inclusivIterator(): Unit = {
    import com.paychex.etc.sterlib.Utils.InclusiveIterator
    assertThat(
      Iterator.apply(1, 2, 3, 4, 5).takeDoWhile(_ < 3).sum,
      is(6) // A regular takeWhile would not include the first failing element (3), so the sum would be 3 instead of 6
    )

    assertThat(
      Iterator.apply(1, 2, 3, 4, 5).takeDoWhile(_ <= 3).sum,
      is(10) // A regular takeWhile would not include the first failing element (4), so the sum would be 6 instead of 10
    )

    val it = Iterator.apply(1, 2, 3, 4, 5).takeDoWhile(_ < 3)
    while(it.hasNext){
      it.next
    }
    try{
      it.next()
      fail("NoSuchElementException was not thrown")
    } catch {
      case e: NoSuchElementException => assertEquals("next on empty iterator", e.getMessage)
    }

  }

  @Test
  def test_defaultValue_Boolean(): Unit = {
    import com.paychex.etc.sterlib.Utils.DefaultValue
    val config: Map[String, String] = Map(
      "existentTrue" -> "true",
      "existentFalse" -> "false",
      "existentInvalid" -> "invalid",
      "empty" -> "",
      "null" -> null
    )
    assertTrue(config("nonExistent") or true)
    assertFalse(config("nonExistent") or false)

    assertTrue(config("existentTrue") or true)
    assertTrue(config("existentTrue") or false)
    assertFalse(config("existentFalse") or true)
    assertFalse(config("existentFalse") or false)

    assertTrue(config("null") or true)
    assertFalse(config("null") or false)

    try {
      config("existentInvalid") or true
      fail("IllegalArgumentException was not thrown")
    } catch {
      case e: IllegalArgumentException => assertEquals("Invalid boolean value: 'invalid'", e.getMessage)
    }

    try {
      config("existentInvalid") or false
      fail("IllegalArgumentException was not thrown")
    } catch {
      case e: IllegalArgumentException => assertEquals("Invalid boolean value: 'invalid'", e.getMessage)
    }

    try {
      config("empty") or false
      fail("IllegalArgumentException was not thrown")
    } catch {
      case e: IllegalArgumentException => assertEquals("Invalid boolean value: ''", e.getMessage)
    }
  }

  @Test
  def test_defaultValue_String(): Unit = {
    import com.paychex.etc.sterlib.Utils.DefaultValue
    val config: Map[String, String] = Map(
      "existent" -> "value", // Should succeed
      "empty" -> "", // Should succeed
      "null" -> null // Should succeed (will be treated as not set)
    )

    assertEquals("value", config("existent") or "ignore")
    assertEquals("", config("empty") or "ignore")
    assertEquals("value", config("null") or "value")
    assertEquals("value", config("nonExistent") or "value")

  }

  @Test
  def test_defaultValue_Int(): Unit = {
    import com.paychex.etc.sterlib.Utils.DefaultValue
    val config: Map[String, String] = Map(
      "existentInt" -> "1", // Should success
      "existentNonInt" -> "a", // Should fail
      "empty" -> "", // Should fail
      "null" -> null // Should success (will be treated as not set)
    )

    assertEquals(1, config("existentInt") or 2)
    assertEquals(1, config("nonExistent") or 1)
    assertEquals(1, config("null") or 1)

    try {
      config("existentNonInt") or 2
      fail("DefaultValue#or should have thrown an exception for attempting to convert a non-integer to an integer")
    } catch {
      case e: IllegalArgumentException => assertThat(e.getMessage, containsString("Invalid number: 'a'"))
    }

    try {
      config("empty") or 2
      fail("DefaultValue#or should have thrown an exception for attempting to convert an empty string to an integer")
    } catch {
      case e: IllegalArgumentException => assertThat(e.getMessage, containsString("Invalid number: ''"))
    }

  }

  @Test
  def test_formatDate(): Unit = {
    import com.paychex.etc.sterlib.Utils.FormatDate
    val date = new Date(1515013206844l) format "yyyy-MM-dd"
    assertEquals(date, "2018-01-03")
  }
}
