package com.paychex.etc.sterlib

import java.io.{ByteArrayInputStream, InputStream}
import java.lang.invoke.MethodHandles
import java.nio.charset.StandardCharsets

import com.paychex.etc.sterlib.util.ThrowMethodException
import org.junit.{Ignore, Test}
import org.junit.Assert._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success, Try}

// FYI: In order to use jUnit from Scala, you have to use a class, not an object.
class SterlibTestScala {
  final def logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

  @Ignore // Ignore since the credentials might not work. Only use for testing.
  def test_push_ecsWlawSelfServe(): Unit = {
    System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug")
    System.setProperty("org.slf4j.simpleLogger.logFile", "System.out")
    val config = Map(
      "method" -> "Ecs",
      "ecsEndpointUrl" -> "https://objlab.paychex.com:9021",
      "ecsNamespace" -> "selfserve",
      "ecsBucketName" -> "",
      "ecsAccessKeyId" -> "wlaw@paychex.com",
      "ecsSecretAccessKey" -> "H3DEquLf4UVu8UVPIpp+e+QU7MPbfug/VrAZFX3l",
      "destinationFilename" -> "wlawTestFilename",
      "sterlingMailboxPath" -> "WLaw_Test",
      "sterlingEndpointUrl" -> "http://sifgsdvw1.paychex.com:15510"
    )
    val data: InputStream = new ByteArrayInputStream("Hello world!\n".getBytes(StandardCharsets.UTF_8))
    if (Try(Sterlib.push(config, data, 0,1)) match {
      case Success(v) => v
      case Failure(_) => false
    }) {
      logger.info("good")
    } else {
      logger.error("bad")
    }
  }

  @Test
  def test_applyConfig(): Unit = {
    val config = Map("method" -> "TrueFalse")
    val func: InputStream => Boolean = Sterlib.applyConfig(config)
    val data = new ByteArrayInputStream("true".getBytes(StandardCharsets.UTF_8))
    assertTrue(func apply data)
  }

  @Test
  def test_push_withNoMarkSupported(): Unit = {
    val config = Map("method" -> "Log")
    val data = new ByteArrayInputStream("someTestData".getBytes("UTF-8")) {
      override def markSupported(): Boolean = false
    }

    val result = Sterlib.push(config, data)
    assertTrue(result)
  }

  @Test(expected = classOf[ThrowMethodException])
  def test_push_withAllAttemptsFailed(): Unit = {
    val config = Map("method" -> "Throw")
    val data = new ByteArrayInputStream("false".getBytes(StandardCharsets.UTF_8))
    Sterlib.push(config, data, 0)
  }

  @Test()
  def test_push_withOneAttemptFailedThenOneSuccess(): Unit = {
    val config = Map("method" -> "Retry", "failuresUntilSuccess" -> "2")
    val data = new ByteArrayInputStream("false".getBytes(StandardCharsets.UTF_8))
    assertTrue(Sterlib.push(config, data, 2, 1))
  }
}