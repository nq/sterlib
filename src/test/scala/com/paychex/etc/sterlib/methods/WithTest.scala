package com.paychex.etc.sterlib.methods

import com.paychex.etc.sterlib.util.With
import org.junit.{Ignore, Test}
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers._



class WithTest {

  @Ignore // Ignore for now until I can get the IntelliJ plugin written.
  def test: Unit = {
    case class SomeClass(
//      @With param: String,
//      @With param2: String
    )
//    val some = SomeClass("param", "param2")
//    assertThat(some.withParam("newParam").param, is("newParam"))
  }
}
