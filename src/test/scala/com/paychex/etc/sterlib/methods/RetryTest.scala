package com.paychex.etc.sterlib.methods

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, OutputStream, PrintStream}

import com.paychex.etc.sterlib.Sterlib
import com.paychex.etc.sterlib.util.OutputCapturer
import org.hamcrest.CoreMatchers._
import org.junit.Assert.{assertThat, assertTrue}
import org.junit.Test

class RetryTest {

  @Test
  def test_push_Retry_Pass(): Unit = {
    OutputCapturer.startCapture()
    val config = Map[String, String](
      "method" -> "Retry",
      "failuresUntilSuccess" -> "1"
    )
    val result = Sterlib.push(config, new ByteArrayInputStream("null".getBytes("UTF-8")), 1, 1)
    val resultString = OutputCapturer.stopCapture()

    assertTrue(result)
    assertTrue(resultString.contains("Trying...")) // Config key
  }
}
