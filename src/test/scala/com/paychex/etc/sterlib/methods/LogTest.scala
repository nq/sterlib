package com.paychex.etc.sterlib.methods

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets

import com.paychex.etc.sterlib.util.OutputCapturer
import org.junit.Test
import org.hamcrest.CoreMatchers._
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue

class LogTest {

  @Test
  def test_Log(): Unit = {
    OutputCapturer.startCapture()

    val data = new ByteArrayInputStream("testData".getBytes(StandardCharsets.UTF_8));
    val config = Map(
      "method" -> "Log",
      "testConfigProperty" -> "testConfigData"
    )
    val result = Log apply config apply data
    val capture = OutputCapturer.stopCapture()

    assertTrue(result)
    assertThat(capture, containsString("Data: testData"))
    assertThat(capture, containsString("testConfigProperty"))
    assertThat(capture, containsString("testConfigData"))

  }
}
