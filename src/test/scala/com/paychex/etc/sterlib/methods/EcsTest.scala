package com.paychex.etc.sterlib.methods

import java.io._
import java.lang.invoke.MethodHandles
import java.net.{InetSocketAddress, URI}
import java.nio.charset.StandardCharsets
import java.util.{Date, UUID}

import com.amazonaws.HttpMethod
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.util.BinaryUtils
import com.paychex.etc.sterlib.util.Utils.HttpRequest
import com.sun.net.httpserver.{HttpExchange, HttpServer}
import org.apache.commons.io.IOUtils
import org.gaul.s3proxy.{AuthenticationType, S3Proxy}
import org.hamcrest.CoreMatchers._
import org.jclouds.ContextBuilder
import org.jclouds.blobstore.domain.StorageMetadata
import org.jclouds.blobstore.options.ListContainerOptions
import org.jclouds.blobstore.{BlobStore, BlobStoreContext}
import org.junit.Assert.{assertEquals, assertThat, assertTrue}
import org.junit.Test
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{Await, Future, Promise}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class EcsTest {
  private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())


  case class EcsTestConfig(
    ecsEndpointHost: String = "localhost",
    var ecsEndpointPort: Int = 0,

    ecsBucketName: String = "someBucketName",
    ecsPresignedBucketName: String = "presigned-inbound",

    ecsAccessKeyId: String = "identity",
    ecsSecretAccessKey: String = "credential",

    ecsFallbackToSterlingNamespace: Boolean = false,
    destinationFilename: String = "someFileName",

    sterlingEndpointHost: String = "localhost",
    var sterlingEndpointPort: Int = 0,
    sterlingMailboxPath: String = "mailbox",

    dataString: String = UUID.randomUUID.toString + UUID.randomUUID.toString,
    presignedFileUri: String = "someRandomFilename",
  ){

    def ecsEndpointUrl: String = s"http://$ecsEndpointHost:$ecsEndpointPort"
    def sterlingEndpointUrl: String = s"http://$sterlingEndpointHost:$sterlingEndpointPort"

    def configMap: Map[String, String] = {
      Map[String, String](
        "method" -> "Ecs",
        "ecsEndpointUrl" -> ecsEndpointUrl,
        "ecsBucketName" -> ecsBucketName,
        "ecsAccessKeyId" -> ecsAccessKeyId,
        "ecsSecretAccessKey" -> ecsSecretAccessKey,
        "destinationFilename" -> destinationFilename,
        "sterlingMailboxPath" -> sterlingMailboxPath,
        "sterlingEndpointUrl" -> sterlingEndpointUrl,
        "ecsFallbackToSterlingNamespace" -> String.valueOf(ecsFallbackToSterlingNamespace)
      )
    }
  }

  @Test
  def test_push_Ecs_withOwnSuccessfulEcsConfiguration(): Unit = {
    // Initialize some configuration values and local services.
    val ecsConfig = EcsTestConfig()

    val ecsServices = EcsServices apply ecsConfig andStart()

    // Presto!
    val result = Ecs apply ecsConfig.configMap apply new ByteArrayInputStream(ecsConfig.dataString getBytes "UTF-8")
    assertTrue(result)

    val results = ecsServices.gatherResults()

    // Verify that we stored in our own namespace
    assertTrue(results.objectMetadata.nonEmpty)
    assertTrue(results.objectData.nonEmpty)
    assertTrue(results.presignedObjectMetadata.isEmpty)
    assertTrue(results.presignedObjectData.isEmpty)

    // Assertions about the sterling call
    assertThat(results.requestToSterling.getDeserializedBody.path("mailboxpath").asText, is(ecsConfig.sterlingMailboxPath))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(ecsConfig.ecsBucketName))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(results.objectMetadata.get.getName))
    assertThat(results.requestToSterling.getDeserializedBody.path("filename").asText, containsString(ecsConfig.destinationFilename))


    // Convert the eTag from base16 to base64
    assertEquals(BinaryUtils.toBase64(BinaryUtils.fromHex(results.objectMetadata.get.getETag)), results.requestToSterling.getDeserializedBody.path("filemd5").asText)
    assertEquals(new URI(ecsConfig.sterlingEndpointUrl + "/file-transfer/submit-file").getPath, results.requestToSterling.getURI.toString)

    // Assertions about the ECS upload.
    assertEquals(ecsConfig.dataString, results.objectData.get)
    assertEquals(ecsConfig.destinationFilename, results.objectMetadata.get.getUserMetadata.get(Ecs.filenameMetadataKeyName))

    ecsServices.stop()
  }

  @Test
  def test_push_Ecs_withPresignedUrlConfiguration(): Unit = {
    // Initialize some configuration values and local services.
    val ecsConfig = EcsTestConfig()
      .copy(ecsBucketName = "")

    val ecsServices = EcsServices apply ecsConfig andStart()

    // Presto!
    val result = Ecs apply ecsConfig.configMap apply new ByteArrayInputStream(ecsConfig.dataString getBytes "UTF-8")
    assertTrue(result)

    val results = ecsServices.gatherResults()

    // Verify that we stored in the pre-signed URL bucket.
    assertTrue(results.objectMetadata.isEmpty)
    assertTrue(results.objectData.isEmpty)
    assertTrue(results.presignedObjectMetadata.nonEmpty)
    assertTrue(results.presignedObjectData.nonEmpty)

    // Assertions about the sterling call
    assertThat(results.requestToSterling.getDeserializedBody.path("mailboxpath").asText, is(ecsConfig.sterlingMailboxPath))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(ecsConfig.ecsPresignedBucketName))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(results.presignedObjectMetadata.get.getName))
    assertThat(results.requestToSterling.getDeserializedBody.path("filename").asText, containsString(ecsConfig.destinationFilename))

    // Convert the eTag from base16 to base64
    assertEquals(BinaryUtils.toBase64(BinaryUtils.fromHex(results.presignedObjectMetadata.get.getETag)), results.requestToSterling.getDeserializedBody.path("filemd5").asText)
    assertEquals(new URI(ecsConfig.sterlingEndpointUrl + "/file-transfer/submit-file").getPath, results.requestToSterling.getURI.toString)

    // Assertions about the ECS upload.
    assertEquals(ecsConfig.dataString, results.presignedObjectData.get)
    assertEquals(ecsConfig.destinationFilename, results.presignedObjectMetadata.get.getUserMetadata.get(Ecs.filenameMetadataKeyName))

    ecsServices.stop()
  }

  @Test
  def test_push_Ecs_withOwnNamespaceButFallBackToPresignedUrl(): Unit = {
    // Initialize some configuration values and local services.
    val initialEcsConfig = EcsTestConfig() copy(ecsFallbackToSterlingNamespace = true)

    val ecsServices = EcsServices apply initialEcsConfig andStart()

    // Change the ECS hostname such that the library fails uploading to our namespace, and instead requests a pre-signed
    // URL from the fake Sterling (which has the correct ECS hostname).
    val ecsConfig = initialEcsConfig.copy(ecsEndpointHost = "someHostThatDoesNotExist")

    // Presto!
    val result = Ecs apply ecsConfig.configMap apply new ByteArrayInputStream(ecsConfig.dataString getBytes "UTF-8")
    assertTrue(result)

    val results = ecsServices.gatherResults()

    // Verify that we stored in the pre-signed URL bucket.
    assertTrue(results.objectMetadata.isEmpty)
    assertTrue(results.objectData.isEmpty)
    assertTrue(results.presignedObjectMetadata.nonEmpty)
    assertTrue(results.presignedObjectData.nonEmpty)

    // Assertions about the sterling call
    assertThat(results.requestToSterling.getDeserializedBody.path("mailboxpath").asText, is(ecsConfig.sterlingMailboxPath))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(ecsConfig.ecsPresignedBucketName))
    assertThat(results.requestToSterling.getDeserializedBody.path("fileurl").asText, containsString(results.presignedObjectMetadata.get.getName))
    assertThat(results.requestToSterling.getDeserializedBody.path("filename").asText, containsString(ecsConfig.destinationFilename))

    // Convert the eTag from base16 to base64
    assertEquals(BinaryUtils.toBase64(BinaryUtils.fromHex(results.presignedObjectMetadata.get.getETag)), results.requestToSterling.getDeserializedBody.path("filemd5").asText)
    assertEquals(new URI(ecsConfig.sterlingEndpointUrl + "/file-transfer/submit-file").getPath, results.requestToSterling.getURI.toString)

    // Assertions about the ECS upload.
    assertEquals(ecsConfig.dataString, results.presignedObjectData.get)
    assertEquals(ecsConfig.destinationFilename, results.presignedObjectMetadata.get.getUserMetadata.get(Ecs.filenameMetadataKeyName))

    ecsServices.stop()
  }

  /** Singleton factory for instantiating the various simulated endpoints to which the library makes calls.
    */
  object EcsServices{

    /** Create and configure the mocked endpoints.
      * @param config [[EcsTestConfig]]
      * @return [[EcsServices]]
      */
    def apply(config: EcsTestConfig): EcsServices  = {
      // Setup a local, in-memory S3 compatible object store.
      val context = ContextBuilder
        .newBuilder("transient")
        .build(classOf[BlobStoreContext])


      // Create the regular bucket.
      // Also create a separate bucket to simulate uploading to Sterling via pre-signed URL.
      val s3Backend = context.getBlobStore
      s3Backend.createContainerInLocation(null, config.ecsBucketName)
      s3Backend.createContainerInLocation(null, config.ecsPresignedBucketName)

      // Setup S3Proxy to forward S3 requests from its listener to the local store
      val s3Proxy = S3Proxy.builder
        .blobStore(context.getBlobStore)
        .endpoint(URI.create(config.ecsEndpointUrl))
        .awsAuthentication(AuthenticationType.AWS_V2_OR_V4, config.ecsAccessKeyId, config.ecsSecretAccessKey)
        .ignoreUnknownHeaders(true)
        .build


      // --------------------------------------------
      //  The two simulated endpoints for testing
      // --------------------------------------------

      // Setup a simple HTTP server to listen for the Sterling requests.
      val sterling = HttpServer.create(new InetSocketAddress(config.sterlingEndpointPort), 0)

      // 1. The first context is the Sterling endpoint to which we will send file metadata (simulating file submission)
      val promisedRequest = Promise[HttpRequest]
      case class Response(code: Int, body: String)

      sterling.createContext("/file-transfer/submit-file", (exchange: HttpExchange) => {
        val parsedRequest = Future(new HttpRequest(exchange))

        promisedRequest.completeWith(parsedRequest)

        val future = parsedRequest.map(_ => Response(200, """{"Status": "Success", "Message": "Okay!"}"""))
        val result = Await.result(future, 3.seconds)

        parsedRequest.onComplete(promisedRequest.tryComplete)
        promisedRequest.tryCompleteWith(parsedRequest)

        exchange.sendResponseHeaders(result.code, result.body.length)
        exchange.getResponseBody write result.body.getBytes(StandardCharsets.UTF_8)
        exchange.close()
      })

      // 2. The second context is the Sterling endpoint from which we will request re-signed URLs
      sterling.createContext("/file-transfer/get-presigned-url", (exchange: HttpExchange) => {
        val responseBody =
        s"""
           |{
           |  "GETURL": "http://www.example.com/${config.ecsPresignedBucketName}/${config.presignedFileUri}",
           |  "PUTURL": "${getPresignedUrl(config)}"
           |}
           """.stripMargin.trim
        exchange.sendResponseHeaders(200, responseBody.length)
        exchange.getResponseBody.write(responseBody.getBytes(StandardCharsets.UTF_8))
        exchange.close()
      })

      new EcsServices(s3Proxy, s3Backend, sterling, promisedRequest.future, config)
    }

    /** Helper function to generate a pre-signed URL for one of the Sterling responders.
      * @param ecsConfig [[EcsTestConfig]] - The test configuration
      * @return [[String]] - A pre-signed URL.
      */
    private def getPresignedUrl(ecsConfig: EcsTestConfig): String = {
      val client = AmazonS3ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(ecsConfig.ecsAccessKeyId, ecsConfig.ecsSecretAccessKey)))
        .withEndpointConfiguration(new EndpointConfiguration(ecsConfig.ecsEndpointUrl, "us-east-1"))
        .withPathStyleAccessEnabled(true)
        .build()

      val expiration = new Date()
      expiration.setTime(expiration.getTime + 1000 * 60 * 60) // Add 1 hour

      assertTrue(client.doesBucketExistV2(ecsConfig.ecsPresignedBucketName))

      val presignedUrl = client.generatePresignedUrl(ecsConfig.ecsPresignedBucketName, ecsConfig.presignedFileUri, expiration, HttpMethod.PUT)
      presignedUrl.toString
    }
  }

  /** Container for the different services used by these tests.
    *
    * @param s3Proxy [[S3Proxy]] - The mocked out ECS
    * @param s3Backend [[BlobStore]] - The underlying data storage for the mocked ECS
    * @param sterling [[HttpServer]] - A mock HTTP server acting as Sterling
    * @param sterlingRequest [[Future]]`[`[[HttpRequest]]] - Contains the value of a future request to the mocked Sterling
    * @param ecsConfig [[EcsTestConfig]] - The test configuration.
    */
  class EcsServices(
    s3Proxy: S3Proxy,
    s3Backend: BlobStore,
    sterling: HttpServer,
    sterlingRequest: Future[HttpRequest],
    ecsConfig: EcsTestConfig
  ){
    def andStart(): EcsServices = {
      s3Proxy.start()
      sterling.start()
      ecsConfig.ecsEndpointPort = s3Proxy.getPort
      ecsConfig.sterlingEndpointPort = sterling.getAddress.getPort
      this
    }

    def stop(): Unit = {
      s3Proxy.stop()
      sterling.stop(0)
    }

    /** Get the results from the mocked services.
      * Some results are Options to represent the uncertainty this context has over which
      * bucket from which we are pulling the results.
      * @return [[EcsResults]] - The results from the mocked services.
      */
    def gatherResults(): EcsResults = {
      // Wait for the REST call to fake sterling to come in. This will keep us from waiting for ECS operations too.
      val requestToSterling = Await.result(sterlingRequest, 3.seconds)

      // Get the metadata of the newly created object directly from the store.
      val objectMetadata = getObjectMetadata(s3Backend, ecsConfig.ecsBucketName)
      val presignedObjectMetadata = getObjectMetadata(s3Backend, ecsConfig.ecsPresignedBucketName)

      // Get the object data itself, and convert it to a string.
      val objectData = getObjectData(objectMetadata, ecsConfig.ecsBucketName)
      val presignedObjectData = getObjectData(presignedObjectMetadata, ecsConfig.ecsPresignedBucketName)

      EcsResults(requestToSterling, objectMetadata, objectData, presignedObjectMetadata, presignedObjectData)
    }

    /** Return either the first metadata in the blob or None
      *
      * @param s3Backend [[BlobStore]] - The mocked service from which to pull the metadata.
      * @param bucketName [[String]] - The bucket name
      * @return [[Option]]`[_<: `[[StorageMetadata]]] - Either the metadata or None
      */
    private def getObjectMetadata(s3Backend: BlobStore, bucketName: String): Option[_ <: StorageMetadata] = {
      val optional = s3Backend
        .list(bucketName, ListContainerOptions.Builder.withDetails())
        .stream()
        .findFirst()
      if(optional.isPresent) Some(optional.get()) else None
    }

    /** Return either the data from the mocked service or None
      *
      * @param objectMetadata [[Option]]`[_<: `[[StorageMetadata]]] - Either the metadata or None
      * @param bucketName [[String]] - The bucket name
      * @return [[Option]]`[`[[String]]] - The object data or None.
      */
    private def getObjectData(objectMetadata: Option[_ <: StorageMetadata], bucketName: String): Option[String] = {
      objectMetadata.map(md =>
        IOUtils.toString(
          s3Backend
            .getBlob(
              bucketName,
              md.getName
            )
            .getPayload
            .openStream(),
          "UTF-8"
        )
      )
    }

    /** Adhoc case class representing the gathered results from the mocked services.
      *
      * @param requestToSterling [[HttpRequest]] - The request sent to the mocked Sterling
      * @param objectMetadata [[Option]]`[_<: `[[StorageMetadata]]] - Either the metadata from our namespace or None
      * @param objectData [[Option]]`[`[[String]]] - The object data from our namespace or None.
      * @param presignedObjectMetadata [[Option]]`[_<: `[[StorageMetadata]]] - Either the metadata from the pre-signed namespace or None
      * @param presignedObjectData [[Option]]`[`[[String]]] - The object data from the pre-signed namespace or None.
      */
    case class EcsResults(
      requestToSterling: HttpRequest,
      objectMetadata: Option[_ <: StorageMetadata],
      objectData: Option[String],
      presignedObjectMetadata: Option[_ <: StorageMetadata],
      presignedObjectData: Option[String]
    )

  }


}
