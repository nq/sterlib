package com.paychex.etc.sterlib.methods

import com.paychex.etc.sterlib.util.OutputCapturer
import org.hamcrest.CoreMatchers._
import org.junit.Assert.{assertThat, assertTrue}
import org.junit.Test

class NoopTest {

  @Test
  def test_push_Noop_returnTrue(): Unit = {
    OutputCapturer.startCapture()

    val config = Map("method" -> "Noop")

    val result: Boolean = Noop apply config apply null
    val capture: String = OutputCapturer.stopCapture

    assertThat(capture, containsString("Method: noop"))
    assertTrue(result)
  }
}
