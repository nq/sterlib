package com.paychex.etc.sterlib.methods

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets

import com.paychex.etc.sterlib.util.OutputCapturer
import org.hamcrest.CoreMatchers._
import org.junit.Assert.{assertThat, assertTrue}
import org.junit.Test

class PrintTest {

  @Test
  def test_push_Print_Pass(): Unit = {
    OutputCapturer.startCapture()
    val testData: String = "This is some test data!"

    val config = Map("method" -> "Print", "testConfig" -> "This is a test config property!")

    val result: Boolean = Print apply config apply new ByteArrayInputStream(testData.getBytes(StandardCharsets.UTF_8))
    val capture: String = OutputCapturer.stopCapture

    assertThat(capture, containsString("testConfig"))
    assertThat(capture, containsString(config("testConfig")))
    assertThat(capture, containsString(testData))
    assertTrue(result)
  }
}
