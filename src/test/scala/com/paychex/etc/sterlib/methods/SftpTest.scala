package com.paychex.etc.sterlib.methods

import java.io._
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import java.util
import java.util.stream.Collectors
import java.util.UUID

import com.paychex.etc.sterlib.util.Utils
import org.apache.sshd.common.file.nativefs.NativeFileSystemFactory
import org.apache.sshd.common.keyprovider.FileKeyPairProvider
import org.apache.sshd.server.SshServer
import org.apache.sshd.server.config.keys.AuthorizedKeysAuthenticator
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory
import org.junit.Assert.{assertEquals, assertFalse, assertTrue}
import org.junit.Test

class SftpTest {

  @Test
  def test_push_Sftp_Pass(): Unit = {
    val hostname = "localhost"
    val sshServerPort = Utils.getFreePort
    val currentDir = new File(".").getAbsolutePath
    val user = "test"
    val privateKeyPath = Utils.getFile("client_private_key").toAbsolutePath.toString
    val timeout = 30
    val knownHostsPath = Utils.getFile("known_hosts").toAbsolutePath.toString

    // Setup a local ssh server to which Sterlib will connect.
    val sshd = SshServer.setUpDefaultServer()
    sshd.setPort(sshServerPort)
    sshd.setKeyPairProvider(new FileKeyPairProvider(Utils.getFile("server_private_key")))
    sshd.setPublickeyAuthenticator(new AuthorizedKeysAuthenticator(Utils.getFile("client_public_key")))
    sshd.setSubsystemFactories(util.Arrays.asList(new SftpSubsystemFactory))
    sshd.setFileSystemFactory(new NativeFileSystemFactory)
    sshd.start()

    // Setup sterlib.
    val filename = "testFilename"
    val dataString = UUID.randomUUID.toString
    val data = new ByteArrayInputStream(dataString.getBytes(StandardCharsets.UTF_8))

    val config = Map[String, String](
      "method" -> "Sftp",
      "destinationServer" -> hostname,
      "destinationPort" -> sshServerPort.toString,
      "sterlingMailboxPath" -> currentDir,
      "destinationFilename" -> filename,
      "user" -> user,
      "privateKeyPath" -> privateKeyPath,
      "timeout" -> timeout.toString,
      "knownHostsPath" -> knownHostsPath
    )

    val result = Sftp apply config apply data

    assertTrue(result)
    assertTrue(Files.exists(Paths.get(filename)))
    // Assert that the data in the file from the ssh server is the same as the data we passed to sterlib.
    assertEquals(new BufferedReader(new InputStreamReader(new FileInputStream(Paths.get(filename).toFile))).lines.collect(Collectors.joining("\n")), dataString)
    // Cleanup
    Files.delete(Paths.get(filename))
    sshd.stop(true)
  }
}
