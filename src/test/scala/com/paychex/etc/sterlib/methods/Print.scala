package com.paychex.etc.sterlib.methods

import java.io.{BufferedInputStream, InputStream}

import com.paychex.etc.sterlib.Sterlib.Method

import scala.io.Source

/**
  * Sample object that extends a function definition to transform it into a top-level function.
  */
object Print extends Method {
  /**
    * Prints out the data stream to standard out and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    def func(data: InputStream): Boolean = {
      val string = Source.fromInputStream(new BufferedInputStream(data)).getLines().mkString("\n")
      println(s"Config: $config")
      println(s"Data: $string")
      true
    }
    func
  }
}
