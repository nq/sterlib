package com.paychex.etc.sterlib.methods

import java.io.InputStream
import java.lang.invoke.MethodHandles

import com.paychex.etc.sterlib.Sterlib.Method
import org.slf4j.{Logger, LoggerFactory}

/**
  * Sample object that extends a function definition to transform it into a top-level function.
  */
object Retry extends Method {
  final def logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
  /**
    * Prints out the data stream to standard out and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    var failures = config("failuresUntilSuccess").toInt
    def func(data: InputStream): Boolean = {
      if(failures == 0){
        logger.info("FINALLY: success")
        return true
      }
      failures = failures - 1
      logger.error("Trying...")
      logger.error("FAILURE! Will try again")
      throw new Exception("Retry: no more retries")
    }
    func
  }
}
