package com.paychex.etc.sterlib.methods

import java.io.InputStream

import com.paychex.etc.sterlib.Sterlib.Method

import scala.language.experimental.macros

/**
  * Sample object that extends a function definition to transform it into a top-level function.
  */
object Noop extends Method {
  /**
    * Calling the whole object calls this method. This method returns a function that will take in a data stream,
    * transfer the file, and return a result. In this case, this is a sample transfer strategy that does nothing; a no-op
    * transmission. It doesn't even print the data, just a random string to signify that it ran and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    def func(data: InputStream): Boolean = {
      println("Method: noop")
      true
    }
    func
  }
}

