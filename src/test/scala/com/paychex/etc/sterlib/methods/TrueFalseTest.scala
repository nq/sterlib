package com.paychex.etc.sterlib.methods

import java.io.ByteArrayInputStream
import org.junit.Assert.{assertFalse, assertTrue}
import org.junit.Test

class TrueFalseTest {

  @Test
  def test_push_TrueFalse_returnTrue(): Unit = {
    val config = Map("Method" -> "TrueFalse")
    val data = new ByteArrayInputStream("true".getBytes("UTF-8"))
    assertTrue(TrueFalse apply config apply data)
  }

  @Test
  def test_push_TrueFalse_returnFalse(): Unit = {
    val config = Map("Method" -> "TrueFalse")
    val data = new ByteArrayInputStream("false".getBytes("UTF-8"))
    assertFalse(TrueFalse apply config apply data)
  }
}
