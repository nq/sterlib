package com.paychex.etc.sterlib.methods

import java.io.{FileInputStream, InputStream}
import java.lang.invoke.MethodHandles

import scala.util.{Failure, Success, Try}
import com.jcraft.jsch.{Channel, ChannelSftp, JSch}
import org.slf4j.{Logger, LoggerFactory}
import com.paychex.etc.sterlib.Sterlib.Method
import com.paychex.etc.sterlib.Utils.DefaultValue


/**
  * Sample object that extends a function definition to transform it into a top-level function.
  */
object Sftp extends Method {
  private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

  /**
    * Prints out the data stream to standard out and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    val sftpConfig = Sftp getConfig config
    def func(data: InputStream): Boolean = {
      logger.debug(s"SFTP configuration: ${sftpConfig.toString}")

      val jsch = new JSch
      jsch.addIdentity(sftpConfig.privateKeyPath)

      Try(jsch.setKnownHosts(new FileInputStream(sftpConfig.knownHostsPath.replaceFirst("^~",System.getProperty("user.home"))) )) match {
        case Success(_) => logger.debug(s"Known hosts loaded from: ${sftpConfig.knownHostsPath}")
        case Failure(_) => logger.warn(s"Failed to load known_hosts file: ${sftpConfig.knownHostsPath}")
      }

      val session = jsch.getSession(sftpConfig.user, sftpConfig.destinationServer, sftpConfig.destinationPort)
      session.setConfig("StrictHostKeyChecking", sftpConfig.strictHostKeyChecking match {
        case true => "yes"
        case false => logger.warn("Strict host key checking has been disabled"); "no"
      })

      session.setConfig("PreferredAuthentications", "publickey")

      try {
        val timeout = sftpConfig.timeout * 1000
        session.connect(timeout)
        val channel = session.openChannel("sftp") match {
          case c: ChannelSftp => c
          case _ => throw new ClassCastException
        }
        channel.setInputStream(System.in)
        channel.setOutputStream(System.out)
        channel.connect(timeout)

        var path = s"${sftpConfig.sterlingMailboxPath}/${sftpConfig.destinationFilename}"
        if (sftpConfig.sterlingMailboxPath.startsWith("~")){
          path = path.replace("~", channel.getHome)
        }

        channel.put(data, path)
        channel.exit()
        session.disconnect()
      } catch {
        case e: Exception =>
          logger.error(s"SFTP connection or transfer error: ${e.getMessage}.")
          session.disconnect()
          throw e
      }
      true
    }
    func
  }

  /**
    * Use the case class to enforce required keys in the config map
    * @param config Map[String, String] - The config map from the consumer of the library
    * @return SftpConfig - The properly filled configuration.
    */
  private def getConfig(config: Map[String, String]) = {
    val sftpConfig = SftpConfig(
      destinationServer = config("destinationServer"),
      privateKeyPath = config("privateKeyPath"),
      destinationPort = config("destinationPort") or 22,
      sterlingMailboxPath = config("sterlingMailboxPath") or "~",
      user = config("user") or System.getProperty("user.name"),
      timeout = config("timeout") or 30,
      knownHostsPath = config("knownHostsPath") or "~/.ssh/known_hosts",
      destinationFilename = config("destinationFilename"),
      strictHostKeyChecking = config("strictHostKeyChecking") or true
    )
    sftpConfig
  }

  /**
    * Defined the required and optional configuration parameters for an SFTP transfer strategy.
    * @param destinationPort Int - The SFTP port to connec to. Default: 22
    * @param destinationServer String - The SFTP hostname to connect to. Required.
    * @param sterlingMailboxPath String - The directory to place the file in. Default: remote user home directory
    * @param user String - The user on the remote to use for authentication. Default: local username
    * @param privateKeyPath String - The local path of the private key for authentication. Required.
    * @param timeout Int - Connection timeout in seconds. Default: 30
    * @param knownHostsPath String - The local path of the known_hosts file. Default: ~/.ssh/known_hosts
    * @param destinationFilename String - The name of the file to be created on the remote host. Required.
    * @param strictHostKeyChecking Boolean - Enable/disable checking the remote host key. Default is set to true.
    */
  private case class SftpConfig(
    destinationServer: String,
    destinationPort: Int,
    sterlingMailboxPath: String,
    user: String,
    privateKeyPath: String,
    timeout: Int,
    knownHostsPath: String,
    destinationFilename: String,
    strictHostKeyChecking: Boolean
  )
}
