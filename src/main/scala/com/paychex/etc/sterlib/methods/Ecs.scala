package com.paychex.etc.sterlib.methods

import scala.util.{Failure, Success, Try}
import scala.xml.XML
import java.io.{ByteArrayInputStream, ByteArrayOutputStream, IOException, InputStream}
import java.lang.invoke.MethodHandles
import java.net.URL
import java.util.logging.LogManager
import java.util.{Date, Properties, UUID}

import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.{AWSStaticCredentialsProvider, BasicAWSCredentials}
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.internal.MD5DigestCalculatingInputStream
import com.amazonaws.services.s3.model._
import com.amazonaws.util.{Base64, BinaryUtils}
import scalaj.http.{Http, HttpOptions}
import org.slf4j.{Logger, LoggerFactory}
import rapture.json._
import rapture.json.jsonBackends.jackson._
import rapture.json.dictionaries.dynamic._
import com.paychex.etc.sterlib.Sterlib.Method
import com.paychex.etc.sterlib.Utils._
import org.apache.commons.io.IOUtils

object Ecs extends Method {
  final def filenameMetadataKeyName: String = "ens-destination-filename"

  private val logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
  /**
    * Prints out the data stream to standard out and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    val ecsConfig = Ecs getConfig config
    (data: InputStream) => {
      logger.debug(s"ECS configuration - ${ecsConfig.toString}")

      // If the ECS namespace properties is not configured properly (intentionally or not), we default to requesting a pre-signed URL from Sterling.
      val ecsFileUrlAndMd5 = if(ecsConfig.ecsBucketName == "" || ecsConfig.ecsAccessKeyId == "" || ecsConfig.ecsSecretAccessKey == ""){
        logger.info("Incomplete ECS configuration. Defaulting to using pre-signed URL from Sterling namespace.")
        uploadToSterlingNamespace(ecsConfig, data)
      // Namespace properties present in the config map, so we're going to try to use our own namespace.
      }else{
        Try(uploadToOwnNamespace(ecsConfig, data)) match {
          case Success(v) => v
          case Failure(e) =>
            if(!ecsConfig.ecsFallbackToSterlingNamespace)
              throw e
            // We failed to upload to our own namespace, so we're going to use the Sterling one instead.
            logger.warn(s"Upload to namespace ${ecsConfig.ecsNamespace} FAILED. Reason: ${e.getMessage}. Falling back to using Sterling's namespace.")
            uploadToSterlingNamespace(ecsConfig, data)
        }
      }

      // We have now uploaded the file into ECS, either in our own namespace, or Sterling's.
      // Either way, the REST call to Sterling looks the same, just with different data.
      val postData =
        s"""
           |{
           |  "mailboxpath": "${ecsConfig.sterlingMailboxPath}",
           |  "fileurl": "${ecsFileUrlAndMd5._1}",
           |  "filename": "${ecsConfig.destinationFilename}",
           |  "filemd5": "${ecsFileUrlAndMd5._2}"
           |}
         """.stripMargin.trim

      // Create the request. Set the timeout a bit higher, since there may be some processing on Sterling's side.
      val request = Http(s"${ecsConfig.sterlingEndpointUrl}/file-transfer/submit-file")
        .method("POST")
        .postData(postData)
        .timeout(30000, 30000)
        .header("content-type", "application/json")

      logger.debug(s"Sterling REST request - \nurl: ${request.url}\nmethod: POST\nheaders: ${request.headers}\npostData: $postData")

      // This actually fires the request off.
      val response = request.asString

      if(response.code < 200 || response.code > 299){ // Success HTTP response codes.
        throw new IOException(s"Sterlib#Ecs - HTTP response code from Sterling: ${response.code}")
      }

      logger.debug(s"Sterling REST response - \ncode: ${response.code}\nbody: \n${response.body}")

      // Parse body, get response status and message.
      val responseBody = Json.parse(response.body)
      val responseStatus = responseBody.Status.as[String]
      val responseMessage = responseBody.Message.as[String]


      // If we don't get a success or a warning, then something is seriously wrong and we should abort.
      if(responseStatus != "Success" && responseStatus != "Warning"){
        throw new IOException(s"Response from Sterling: $responseMessage")
      }
      true
    }
  }


  /**
    * Upload the file to Sterling's ECS namespace by requesting a pre-signed upload URL from them.
    * @param ecsConfig EcsConfig - The config object
    * @param data InputStream - The data stream to be uploaded.
    * @return (URL, String) - A tuple containing the URL and the MD5 has of the file.
    */
  private def uploadToSterlingNamespace(ecsConfig: EcsConfig, data: InputStream): FileUpload = {

    logger.debug("Requesting pre-signed URL")
    val presignedUrlResponse = Http(s"${ecsConfig.sterlingEndpointUrl}/file-transfer/get-presigned-url?fmt=json")
      .timeout(30000,30000)
      .option(HttpOptions.followRedirects(true))
      .asString

    val jsonResponse = Json.parse(presignedUrlResponse.body)
    val getUrl = jsonResponse.GETURL.as[String] // We give this back to Sterling
    val putUrl = jsonResponse.PUTURL.as[String] // We use this to upload our file

    logger.debug(s"Pre-signed URL response: ${presignedUrlResponse.toString}")

    // Calculate MD5 hash for goodness sake, but do it in-line.
    val md5DigestStream = new MD5DigestCalculatingInputStream(data)


    logger.debug(s"Uploading file to pre-signed URL: $putUrl")

    val uploadResponse = Http(putUrl)
      .put(IOUtils.toByteArray(md5DigestStream))
      .option(HttpOptions.followRedirects(true))
      .timeout(300000, 300000)
      .header(s"x-amz-meta-$filenameMetadataKeyName", ecsConfig.destinationFilename)
      .asString

    if(uploadResponse.code < 200 || uploadResponse.code > 299){ // Success HTTP response codes.
      logger.debug(uploadResponse.toString)
      throw new IOException(s"Error uploading to ECS: ${uploadResponse.code}")
    }

    // Convert the computed MD5 and ECS's MD5 to a Base64-encoded string.
    val computedMd5 = Base64.encodeAsString(md5DigestStream.getMd5Digest: _*)
    val responseMd5 = uploadResponse.header("ETag") match {
      case Some(s) => BinaryUtils.toBase64(BinaryUtils.fromHex(s.replace("\"", "")))
      case None => throw new IOException("Upload to ECS failed, no computed MD5 found from response.")
    }

    // Compare the MD5. If they don't match, throw an error.
    if(computedMd5 != responseMd5){
      throw new IOException("Unable to verify integrity of data upload. Client " +
        s"calculated content hash ($computedMd5) did not match " +
        s"the has calculated by ECS ($responseMd5).")
    }

    // Return a tuple from this "if expression" (Scala's version of a ternary operator)
    (new URL(getUrl), computedMd5)
  }

  /**
    * Upload the file to our own ECS namespace that we have credentials for.
    * @param ecsConfig EcsConfig - The config object
    * @param data InputStream - The data stream to be uploaded.
    * @return (URL, String) - A tuple containing the URL and the MD5 has of the file.
    */
  private def uploadToOwnNamespace(ecsConfig: EcsConfig, data: InputStream): FileUpload = {
    val ecsHost = (
      for (m <- Regex.url.findFirstMatchIn(ecsConfig.ecsEndpointUrl)) yield m group "host"
      ) match {
      case Some(s) => s
      case None => throw new Exception(s"Invalid ECS URL ${ecsConfig.ecsEndpointUrl}")
    }

    val ecsEndpointUrl = if(ecsHost.contains(ecsConfig.ecsNamespace)) {
      ecsConfig.ecsEndpointUrl
    } else {
      ecsConfig.ecsEndpointUrl.replace(ecsHost, s"${ecsConfig.ecsNamespace}.$ecsHost")
    }

    val client = AmazonS3ClientBuilder.standard()
      .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(ecsConfig.ecsAccessKeyId, ecsConfig.ecsSecretAccessKey)))
      .withEndpointConfiguration(new EndpointConfiguration(ecsEndpointUrl, "us-east-1"))
      .withPathStyleAccessEnabled(true)
//      .withClientConfiguration(new ClientConfiguration().withProxyHost("127.0.0.1").withProxyPort(3128))
      .build()

    if(!client.doesBucketExistV2(ecsConfig.ecsBucketName)){
      logger.warn(s"Bucket ${ecsConfig.ecsBucketName} does not exist. Creating...")
      val createBucketRequest = new CreateBucketRequest(ecsConfig.ecsBucketName).withCannedAcl(CannedAccessControlList.BucketOwnerFullControl)
      val result = Try(client.createBucket(createBucketRequest)) match {
        case Success(v) => logger.info(s"Successfully created new bucket: ${ecsConfig.ecsBucketName}"); v
        case Failure(e) => logger.error("Error creating bucket"); throw e
      }
    }

    val metadata = new ObjectMetadata()
    metadata.addUserMetadata(filenameMetadataKeyName, ecsConfig.destinationFilename)

    val acl = new AccessControlList()
    acl.grantPermission(new CanonicalGrantee(ecsConfig.ecsAccessKeyId), Permission.FullControl)

    val fileId = UUID.randomUUID.toString
    //      val fileUri = s"${new Date().format("yyyy-MM-dd")}/$fileId"
    val fileUri = fileId // For now, we will just use the UUID as the URI.
    val ecsPutRequest = new PutObjectRequest(ecsConfig.ecsBucketName, fileUri, data, metadata).withAccessControlList(acl)


    logger.debug(s"ECS Request - bucket: ${ecsConfig.ecsBucketName}, fileUri: $fileUri, data: $data, metadata: $metadata, acl: $acl")

    val result = Try(client.putObject(ecsPutRequest)) match {
      case Success(v) => logger.info(s"File transfer to ECS succeeded ($fileUri)"); v
      case Failure(e) => logger.error(s"File transfer ECS failed!"); throw e
    }

    val expiration = new Date()
    expiration.setTime(expiration.getTime + 1000 * 60 * 60) // Add 1 hour

    val presignedUrl = client.generatePresignedUrl(ecsConfig.ecsBucketName, fileUri, expiration)

    (presignedUrl, result.getContentMd5)
  }

  /**
    * Just a type alias to unify the return type semantics for the different upload methods.
    */
  type FileUpload = (URL, String)

  /**
    * Use the case class to enforce required keys in the config map
    * @param config Map[String, String] - The config map from the consumer of the library
    * @return EcsConfig - The properly filled configuration.
    */
  private def getConfig(config: Map[String, String]) = {
    val sftpConfig = EcsConfig(
      ecsEndpointUrl = config("ecsEndpointUrl") or "https://objlab.paychex.com:9021",
      ecsAccessKeyId = config("ecsAccessKeyId") or "",
      ecsSecretAccessKey = config("ecsSecretAccessKey") or "",
      ecsNamespace = config("ecsNamespace") or "",
      ecsBucketName = config("ecsBucketName") or "",
      ecsFallbackToSterlingNamespace = config("ecsFallbackToSterlingNamespace") or true,
      destinationFilename = config("destinationFilename"),
      sterlingMailboxPath = config("sterlingMailboxPath"),
      sterlingEndpointUrl = config("sterlingEndpointUrl") or "http://sifgsdvw1.paychex.com:15510"
    )
    sftpConfig
  }

  /**
    * Defined the required and optional configuration parameters for an SFTP transfer strategy.
//    * @param getPresignedBucketUrl Boolean - Get a pre-signed ECS url from Sterling to use for the upload to ECS.
    * @param ecsEndpointUrl String - The ECS endpoint to connect to.
    * @param ecsNamespace String - The ECS namespace to connect to. Default is empty string (ECS will select the default namespace).
    * @param ecsAccessKeyId String - The username/access key ID to use to connect to the ECS instance with.
    * @param ecsSecretAccessKey String - The password/secret access key that corresponds with the key ID.
    * @param ecsFallbackToSterlingNamespace Boolean - Whether or not to fallback to using the Sterling namespace if the upload to your own namespace fails.
    * @param destinationFilename String - The filename that the bank or vendor partner is expecting.
    * @param sterlingMailboxPath String - The Sterling path/route to use to transmit the file to the partner.
    * @param sterlingEndpointUrl String - The Sterling endpoint URL.
    */
  private case class EcsConfig(
    ecsEndpointUrl: String,
    ecsNamespace: String,
    ecsBucketName: String,
    ecsAccessKeyId: String,
    ecsSecretAccessKey: String,
    ecsFallbackToSterlingNamespace: Boolean,
    destinationFilename: String,
    sterlingMailboxPath: String,
    sterlingEndpointUrl: String
  )
}
