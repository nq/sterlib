package com.paychex.etc.sterlib.methods

import java.io.{BufferedInputStream, InputStream}
import java.lang.invoke.MethodHandles

import com.paychex.etc.sterlib.Sterlib.Method
import org.apache.commons.io.IOUtils
import org.slf4j.{Logger, LoggerFactory}

import scala.io.Source

/**
  * Sample object that extends a function definition to transform it into a top-level function.
  */
object Log extends Method {
  final def logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
  /**
    * Prints out the data stream to standard out and returns true.
    * @param config Map[String, String] - The configuration needed by the implementation.
    * @return InputStream => Boolean - A function that takes performs the data transmission.
    */
  override def apply(config: Map[String, String]): InputStream => Boolean = {
    def func(data: InputStream): Boolean = {
      val string = IOUtils.toString(data, "UTF-8")
      logger.info(s"Config: $config")
      logger.info(s"Data: $string")
      true
    }
    func
  }
}
