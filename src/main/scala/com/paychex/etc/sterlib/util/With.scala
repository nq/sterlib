package com.paychex.etc.sterlib.util

import scala.annotation.{StaticAnnotation, compileTimeOnly}
import scala.language.experimental.macros
import scala.reflect.macros.blackbox.Context

@compileTimeOnly("enable macro paradise to expand macro annotations")
class With extends StaticAnnotation {
  def macroTransform(annottees: Any*): Any = macro With.generate
}

object With {
  def generate(c: Context)(annottees: c.Expr[Any]*) = {
    import c.universe._
    val result = annottees.map(_.tree).toList match {
      case (q"$_ val $tname: $tpt = $_") :: ((q"case class $tpname[..$tparams] $ctorMods(...$paramss) extends { ..$earlydefns } with ..$parents { $self => ..$stats }") :: Nil) => {
        val withFunctionName = TermName("with" + tname.toString().capitalize)
        val withFunction = q"def $withFunctionName($tname: $tpt): $tpname = {copy($tname = $tname)};"
        q"""case class $tpname[..$tparams] $ctorMods(...$paramss) extends { ..$earlydefns } with ..$parents {
          $self => ..$stats
          $withFunction
        }"""
      }
      case _ => c.abort(c.enclosingPosition, "Annotation @With can be used only with case classes")
    }
    q"$result"
  }
}
