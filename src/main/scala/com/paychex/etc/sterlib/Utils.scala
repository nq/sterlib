package com.paychex.etc.sterlib

import java.text.SimpleDateFormat
import java.util.Date

import scala.collection.{AbstractIterator, Iterator}
import scala.collection.Iterator.empty
import scala.util.{Try, Failure, Success}
import scala.util.control.Exception

/**
  * Utility classes, functions, and objects for Sterlib
  */
object Utils{

  /**
    * Add a method for the Iterator that keeps the last element that failed the predicate for takeWhile()
    * @param it Iterator[A] - The original iterator that is being read from.
    * @tparam A - The type that the iterator is iterating over.
    */
  implicit final class InclusiveIterator[A](it: Iterator[A]){
    /**
      * The actual function to act upon the iterator. Emuluates a "do-while" loop. In other words, it will stop iterating on
      * predicte failure, but will include that last result as a part of the iterator.
      * @param predicate {A => Boolean} - A function that is evaluated on each result from the parent iterator.
      * @return
      */
    @inline def takeDoWhile(predicate: A => Boolean): Iterator[A] = new AbstractIterator[A] {
      private var continue: Boolean = true

      /**
        * True if the parent iterator is true AND the predicate has not already failed.
        * @return
        */

      def hasNext: Boolean = it.hasNext && continue

      /**
        * If the predicate fails, we return that item, but mark this iterator to fail on the next iteration.
        * @return
        */
      def next(): A = {
        if(continue){
          val result: A = it.next()
          if (!predicate(result)) continue = false
          result // Give the result even if the predicate fails, but only once.
        }else{
          empty.next()
        }
      }
    }
  }

  /**
    * A simple way to specify a default value for Method configuration case classes.
    * @param body {() => String} - A function to be executed, usually a key lookup in a map.
    */
  implicit final class DefaultValue(body: => String) {
    /**
      * If the "else" of the "or else" is supposed to be an integer. Convert the string to an Integer or use the default value if that fails.
      * @param Else
      * @return
      */
    @inline def or (Else: Int): (Int) = {
      Exception.catching(classOf[NoSuchElementException]) opt body filterNot(_ == null) match {
        case Some(value) => Try(value.toInt) match {
          case Success(int) => int
          case Failure(e) => throw new IllegalArgumentException(s"Invalid number: '$value'", e)
        }
        case None => Else
      }
    }

    /**
      * If the value is supposed to be a String. Execute the body, make sure the string is not null or empty. Otherwise use default value.
      * @param Else
      * @return
      */
    @inline def or (Else: String): (String) = {
      Exception.catching(classOf[NoSuchElementException]) opt body filterNot (_ == null) getOrElse Else
    }

    /**
      * If the value is supposed to be a boolean. Execute body, ensure non-null and non-empty string. Get boolean value of string, otherwise use default.
      * @param Else
      * @return
      */
    @inline def or (Else: Boolean): (Boolean) = {
      Exception.catching(classOf[NoSuchElementException]) opt body filterNot(_ == null) match {
        case Some(value) => value match {
          case "true" => true
          case "false" => false
          case _ => throw new IllegalArgumentException(s"Invalid boolean value: '$value'")
        }
        case None => Else
      }
    }
  }

//  implicit final class ConfigFormatter

  implicit final class FormatDate(date: => Date){
    @inline def format (format: String): (String) = new SimpleDateFormat(format).format(date)
  }

  object Regex {
    def url =
      """
        |^
        |(?:([^:/?#]+):\/\/)?
        |(?:([a-zA-Z0-9\-_.+]+)(?:\:([a-zA-Z0-9\-_.+]+))?@)?
        |((?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)(?:\.(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))*\.?)
        |(?:\:(6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{1,3}|[0-9]))?
        |(\/[^?#]*)?
        |(?:\?([^#]*))?
        |(?:#(.*))?
        |$
      """.stripMargin.replace("\n", "").trim.r("protocol", "user", "password", "host", "port", "path", "query", "selector")
  }

}
