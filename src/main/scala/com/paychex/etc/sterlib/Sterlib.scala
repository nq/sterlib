package com.paychex.etc.sterlib

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, InputStream}
import java.lang.invoke.MethodHandles
import java.nio.charset.StandardCharsets

import scala.reflect.runtime.universe
import scala.collection.JavaConverters._
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.Iterator
import scala.util.{Failure, Success, Try}
import com.paychex.etc.sterlib.Utils.InclusiveIterator
import org.apache.commons.io.IOUtils

/**
  * The main library class that does it all :)
  */
object Sterlib {
  final def logger: Logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

  /**
    * Given a string that represents the name of a transmission method, returns the function that performs the transmission.
    * @param method String - A classname that holds the implementation of the transfer functionality
    * @return Map[String, String] => InputStream => Boolean - The function that will perform the transfer.
    */
  private def getAction(method: String) = {
    val mirror = universe.runtimeMirror(getClass.getClassLoader)
    val module = mirror.staticModule(s"com.paychex.etc.sterlib.methods.$method")
    val obj = mirror.reflectModule(module)
    obj.instance.asInstanceOf[Method]
  }

  /**
    * Define the type alias for what a transmission method should look like.
    */
  type Method = Map[String, String] => InputStream => Boolean

  /* ******************************************************
   *  Scala API Definition:
   * *****************************************************/

  /**
    * Scala:
    * The main entry-point into the library for Scala consumers.
    * @param config Map[String, String] - A Map that contains any necessary configuration properties for the method of transfer.
    * @param data InputStream - A Stream that represents a reference to some arbitrary source of data that will be sent to Sterling
    * @param retries Int - Number of attempts to make before failing.
    * @param retryInterval Int - Time in seconds between each retry.
    * @return Boolean - The status of the transmission.
    */
  def push(config: Map[String, String], data: InputStream, retries: Int = 2, retryInterval: Int = 30): Boolean = {
    val action = getAction(config("method"))
    val configuredAction = action apply config

    // We need to determine if we can reset the input stream if we need to retry. If we can't, then we need to copy the data in the input stream.
    val releasedData = if(data.markSupported() || retries == 0) data else {
      val baos = new ByteArrayOutputStream()
      IOUtils.copy(data, baos)
      new ByteArrayInputStream(baos.toByteArray)
    }

    Iterator
      .continually(Try(configuredAction apply releasedData))
      .zip((retries to 0 by -1).iterator) // Turn the Successes/Failures into tuples, with the Successes/Failures as the first element and index as the second.
      .takeDoWhile {
        case (Failure(e), 0) => // If we fail and its the final attempt (retries left = 0)
          if(logger.isDebugEnabled) e.printStackTrace()
          logger.error(s"Final attempt to submit file to Sterling failed: ${e.getMessage}")
          true
        case (Failure(e), i) => // If we fail but we still have some retries remaining
          if(logger.isDebugEnabled) e.printStackTrace()
          logger.error(s"Attempt ${retries-i+1} to submit file to Sterling failed: ${e.getMessage}")
          logger.error(s"Retrying in $retryInterval second(s)...")
          releasedData.reset()
          Thread.sleep(retryInterval*1000)
          true
        case (Success(_), _) => false
      }
      .map(_._1) // Convert the tuples to just the Successes/Failures
      .take(Math.max(0, retries+1))
      .reduceLeft {(left, right) =>
        if(right.isSuccess) right
        else left
      } match {
      case Success(v) => logger.info("File successfully transmitted to Sterling");v
      case Failure(e) => logger.error(s"Error pushing to Sterling: ${e.getMessage}"); throw e
    }
  }

  /**
    * Scala:
    * Curries the transmission function with the configuration applied, returning a function that
    * transmits the data stream and returns a boolean.
    * @param config Map[String, String] - A Map that contains any necessary configuration properties for the method of transfer.
    * @return InputStream => Boolean - A function that transmits the data and returns a boolean.
    */
  def applyConfig(config: Map[String, String]): InputStream => Boolean = {
    val action = getAction(config("method"))
    action apply config
  }

  /* ******************************************************
   *  Java API Definition:
   * *****************************************************/

  /**
    * Java:
    * The main entry-point into the library for Java consumers.
    * @param config java.util.Map[String, String] - A Map that contains any necessary configuration properties for the method of transfer.
    * @param data InputStream - A Stream that represents a reference to some arbitrary source of data that will be sent to Sterling
    * @return Boolean - The status of the transmission.
    */
  def push(config: java.util.Map[String, String], data: InputStream): Boolean = {
    push(config.asScala.toMap, data)
  }

  /**
    * Java:
    * The main entry-point into the library for Java consumers.
    * @param config java.util.Map[String, String] - A Map that contains any necessary configuration properties for the method of transfer.
    * @param data InputStream - A Stream that represents a reference to some arbitrary source of data that will be sent to Sterling
    * @param retries int - Number of attempts to make before failing.
    * @return Boolean - The status of the transmission.
    */
  def push(config: java.util.Map[String, String], data: InputStream, retries: Int): Boolean = {
    push(config.asScala.toMap, data, retries)
  }

  /**
    * Java:
    * The main entry-point into the library for Java consumers.
    * @param config java.util.Map[String, String] - A Map that contains any necessary configuration properties for the method of transfer.
    * @param data InputStream - A Stream that represents a reference to some arbitrary source of data that will be sent to Sterling
    * @param retries int - Number of attempts to make before failing.
    * @param retryInterval int - Time in seconds between each retry.
    * @return Boolean - The status of the transmission.
    */
  def push(config: java.util.Map[String, String], data: InputStream, retries: Int, retryInterval: Int): Boolean = {
    push(config.asScala.toMap, data, retries, retryInterval)
  }

}
